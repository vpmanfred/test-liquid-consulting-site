import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PageListComponent} from "./page-list/page-list.component";
import {PageDetailComponent} from "./page-detail/page-detail.component";

const routes: Routes = [
  { path: '', component: PageListComponent },
  { path: 'details/:action/:studentId', component: PageDetailComponent },
  { path: 'list', redirectTo: '' },
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
