import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {SiteBoolean} from "../shared/models/siteBoolean.model";
import {ActivatedRoute, Router} from "@angular/router";
import {isNumeric} from "rxjs/internal-compatibility";
import Swal, { SweetAlertOptions } from "sweetalert2";
import {ApiService} from "src/app/shared/services/api.service";
import {Student} from "src/app/shared/models/student.model";
import {SiteGender} from "src/app/shared/models/siteGender.model";
import {DatePipe} from "@angular/common";

@Component({
  selector: 'app-page-detail',
  templateUrl: './page-detail.component.html',
  styleUrls: ['./page-detail.component.scss']
})
export class PageDetailComponent implements OnInit {

  majors: string[] = ["Administracion", "Informatica", "Medicina", "Contabilidad", "Odontologia"];
  genders: SiteGender[] = [{name: "Hombre", english: "male"}, {name: "Mujer", english: "female"}];
  isActiveValues: SiteBoolean[] = [{name: "Sí", value: true}, {name: "No", value: false}];
  currentAction: string = "";
  currentStudentId: number = 0;
  currentStudent: Student = <Student>{};

  public studentDetailFormGroup: FormGroup;

  constructor(private route: ActivatedRoute, private formBuilder: FormBuilder, private router: Router, private apiService: ApiService, public datepipe: DatePipe) {
    this.studentDetailFormGroup = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      dateOfBirth: ['', [Validators.required]],
      gender: ['', [Validators.required]],
      university: ['', [Validators.required]],
      major: [''],
      dateOfAdmission: [''],
      address: [''],
      isActive: ['', [Validators.required]],
    });

    this.route.params.subscribe(params => {
      if (params['action'] && params['studentId']) {
        if ((params['action'] != 'Update' && params['action'] != 'Create') || !isNumeric(params['studentId']))
        {
          this.router.navigate(['/list']);
        }
        this.currentAction = params['action'];
        this.currentStudentId = parseInt(params['studentId']);

        if(params['action'] == 'Create')
          return;

        this.apiService.getStudentsById(this.currentStudentId).subscribe((data)=> {
          // /*debug*/ console.log(data);
          this.currentStudent = data.payload;
          if(this.currentStudent) {
            this.studentDetailFormGroup.controls['firstName'].setValue(this.currentStudent.first_name);
            this.studentDetailFormGroup.controls['lastName'].setValue(this.currentStudent.last_name);
            this.studentDetailFormGroup.controls['dateOfBirth'].setValue(new Date(this.currentStudent.date_of_birth));
            this.studentDetailFormGroup.controls['gender'].setValue(this.currentStudent.gender);
            this.studentDetailFormGroup.controls['university'].setValue(this.currentStudent.university);
            this.studentDetailFormGroup.controls['major'].setValue(this.currentStudent.major);
            this.studentDetailFormGroup.controls['dateOfAdmission'].setValue(new Date(this.currentStudent.date_of_admission));
            this.studentDetailFormGroup.controls['address'].setValue(this.currentStudent.address);
            this.studentDetailFormGroup.controls['isActive'].setValue(this.currentStudent.is_active);

            let selectedGender = this.genders.find(i => i.english == this.currentStudent.gender);
            if(selectedGender) {
              this.studentDetailFormGroup.controls['gender'].setValue(selectedGender.english);
            }
            let selectedStatus = this.isActiveValues.find(i => i.value === this.currentStudent.is_active);
            if(selectedStatus) {
              this.studentDetailFormGroup.controls['isActive'].setValue(selectedStatus.value);
            }
          }
        });
      }
    });
  }

  ngOnInit(): void {
  }

  public hasError = (controlName: string, errorName: string) => {
    return this.studentDetailFormGroup.controls[controlName].hasError(errorName);
  };

  UpdateOrCreateStudent() {
    if (this.studentDetailFormGroup.valid) {
      let newStudentInfo:Student = <Student>{};
      newStudentInfo.first_name = this.studentDetailFormGroup.controls['firstName'].value;
      newStudentInfo.last_name = this.studentDetailFormGroup.controls['lastName'].value;
      newStudentInfo.date_of_birth = <string>this.datepipe.transform(this.studentDetailFormGroup.value.dateOfBirth, 'MM-dd-yyyy');
      newStudentInfo.gender = this.studentDetailFormGroup.controls['gender'].value;
      newStudentInfo.university = this.studentDetailFormGroup.controls['university'].value;
      newStudentInfo.major = this.studentDetailFormGroup.controls['major'].value;
      newStudentInfo.date_of_admission = this.studentDetailFormGroup.controls['dateOfAdmission'].value;
      newStudentInfo.address = this.studentDetailFormGroup.controls['address'].value;
      newStudentInfo.is_active = this.studentDetailFormGroup.controls['isActive'].value;

      if(this.currentAction == "Update") {
        newStudentInfo.student_id = this.currentStudent.student_id;
        // /*debug*/ console.log(newStudentInfo);
        this.apiService.putUpdateStudent(newStudentInfo).subscribe((data)=> {
          // /*debug*/ console.log(data);
          if(data.success == "true") {
            Swal.fire(
              'Éxito!',
              'El estudiante ha sido actualizado correctamente!',
              'success'
            );
            this.router.navigate(['/list']);
          }
        });
      } else if(this.currentAction == "Create") {
        // /*debug*/ console.log(newStudentInfo);
        this.apiService.postAddStudent(newStudentInfo).subscribe((data)=> {
          // /*debug*/ console.log(data);
          if(data.success == "true") {
            Swal.fire(
              'Éxito!',
              'El estudiante ha sido ingresado correctamente!',
              'success'
            );
            this.router.navigate(['/list']);
          }
        });
      }
    }
  }

}
