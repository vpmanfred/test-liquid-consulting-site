import { Component, OnInit } from '@angular/core';
import {Student} from "../shared/models/student.model";
import {ApiService} from "src/app/shared/services/api.service";
import Swal from "sweetalert2";

@Component({
  selector: 'app-page-list',
  templateUrl: './page-list.component.html',
  styleUrls: ['./page-list.component.scss']
})
export class PageListComponent implements OnInit {

  displayedColumns: string[] = ['first_name', 'last_name', 'date_of_birth', 'gender', 'university', 'edit', 'delete'];

  students: Student[] = [
    // {student_id:1, first_name: "Larry", last_name:"King", date_of_birth:"10/12/1980", gender:"Hombre", university:"Oxford", major:"Marketing", date_of_admission:"02/05/2000", address:"Doral, Florida", is_active:true},
    // {student_id:2, first_name: "Barry", last_name:"King", date_of_birth:"10/12/1980", gender:"Hombre", university:"Oxford", major:"Marketing", date_of_admission:"02/05/2000", address:"Doral, Florida", is_active:true},
    // {student_id:3, first_name: "Harry", last_name:"King", date_of_birth:"10/12/1980", gender:"Hombre", university:"Oxford", major:"Marketing", date_of_admission:"02/05/2000", address:"Doral, Florida", is_active:false},
    // {student_id:4, first_name: "Nancy", last_name:"King", date_of_birth:"10/12/1980", gender:"Mujer", university:"Oxford", major:"Marketing", date_of_admission:"02/05/2000", address:"Doral, Florida", is_active:true},
    ];

  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    this.getStudents();
  }

  deleteStudent (studentId: number){
    // /*debug*/ console.log(studentId);
    this.apiService.postDeleteStudent(studentId).subscribe((data)=> {
      // /*debug*/ console.log(data);
      this.students = data.payload;
      if(data.success == "true") {
        Swal.fire(
          'Éxito!',
          'El estudiante ha sido removido correctamente!',
          'success'
        );
        this.getStudents();
      }
    });
  }

  getStudents() {
    this.apiService.getStudents().subscribe((data)=> {
      // /*debug*/ console.log(data);
      this.students = data.payload;
    });
  }

}
