import { Injectable } from '@angular/core';
import {environment} from 'src/environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Store} from '@ngrx/store';
import {ApiResponse} from "src/app/shared/models/api-response.model";
import {Student} from "src/app/shared/models/student.model";

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private apiUrl = environment.backendDomain;

  constructor(private httpClient: HttpClient) { }

  public getStudents() {
    return this.httpClient.get<ApiResponse>('http://localhost:3000/students');
  }

  public getStudentsById(studentId: number) {
    const url = 'http://localhost:3000/getStudent/' + studentId;
    return this.httpClient.get<ApiResponse>(url);
  }

  public putUpdateStudent(student: Student) {
    const url = 'http://localhost:3000/updateStudent/'+student.student_id;
    let params = new HttpParams();
    params = params.set("student_id", student.student_id.toString());
    params = params.set("first_name", student.first_name);
    params = params.set("last_name", student.last_name);
    params = params.set("date_of_birth", student.date_of_birth);
    params = params.set("gender", student.gender);
    params = params.set("university", student.university);
    params = params.set("major", student.major);
    params = params.set("date_of_admission", student.date_of_admission);
    params = params.set("address", student.address);
    params = params.set("is_active", student.is_active.toString());
    return this.httpClient.put<ApiResponse>(url, params);
  }

  public postAddStudent(student: Student) {
    const url = 'http://localhost:3000/addStudent';
    let params = new HttpParams();
    params = params.set("first_name", student.first_name);
    params = params.set("last_name", student.last_name);
    params = params.set("date_of_birth", student.date_of_birth);
    params = params.set("gender", student.gender);
    params = params.set("university", student.university);
    params = params.set("major", student.major);
    params = params.set("date_of_admission", student.date_of_admission);
    params = params.set("address", student.address);
    params = params.set("is_active", student.is_active.toString());
    return this.httpClient.post<ApiResponse>(url, params);
  }

  public postDeleteStudent(studentId: number) {
    const url = 'http://localhost:3000/deleteStudent/' + studentId;
    return this.httpClient.delete<ApiResponse>(url);
  }
}
