import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'gender'
})
export class genderPipe implements PipeTransform {

  booleans: { name: String, value: String }[] = [{name: 'Hombre', value: "male"}, {name: 'Mujer', value: "female"}];

  constructor() {
  }

  transform(value: any, args?: any): any {
    const itemList = this.booleans.filter(item => item.value === value);
    if (itemList.length > 0) {
      return itemList[0].name;
    } else {
      return value;
    }
  }

}
