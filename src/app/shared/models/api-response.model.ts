export interface ApiResponse {
  success: string;
  message: string;
  payload: any;
}
