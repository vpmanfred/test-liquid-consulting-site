export interface SiteGender {
  name: string;
  english: string;
}
