export interface SiteBoolean {
  name: string;
  value: boolean;
}
