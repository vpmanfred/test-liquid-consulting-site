export interface Student {
  student_id: number;
  first_name: string;
  last_name: string;
  date_of_birth: string;
  gender: string;
  university: string;
  major: string;
  date_of_admission: string;
  address: string;
  is_active: boolean;
}
